FROM php:7-alpine

WORKDIR /

RUN docker-php-ext-install pdo pdo_mysql

COPY pdo_mysql.ini usr/local/etc/php/conf.d
