<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'db' => [
                'host' => getenv('DB_HOST'),
                'dbname' => getenv('DB_NAME'),
                'user' => getenv('DB_USERNAME'),
                'pass' => getenv('DB_PASSWORD')
            ],
            'api' => [
                'openWeatherMap' => [
                    'baseUri' => 'https://api.openweathermap.org/data/2.5/',
                    'apiKey' => getenv('OPEN_WEATHER_MAP_API_KEY')
                ],
                'weatherbit' => [
                    'baseUri' => 'https://api.weatherbit.io/v2.0/',
                    'apiKey' => getenv('WEATHERBIT_API_KEY')
                ],
                'weatherApi' => [
                    'baseUri' => 'https://api.weatherapi.com/v1/',
                    'apiKey' => getenv('WEATHER_API_API_KEY')
                ]
            ],
            'jwt' => [
                'serverName' => 'servername',
                'secretKey' => 'secretkey'
            ]
        ],
    ]);
};
