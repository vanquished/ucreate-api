<?php
declare(strict_types=1);

use App\Application\Actions\User\CreateUserAction;
use App\Application\Actions\User\LoginUserAction;
use App\Application\Actions\User\UpdateUserCityAction;
use App\Application\Actions\City\ListCitiesAction;
use App\Application\Actions\City\ViewCityAction;
use App\Application\Actions\City\CreateCityAction;
use App\Application\Actions\Weather\ViewWeatherForecastAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Application\Middleware\JwtAuthMiddleware;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

return function (App $app) {

    $app->addBodyParsingMiddleware();

    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->put('', CreateUserAction::class);
        $group->post('/login', LoginUserAction::class);
        $group->post('/city', UpdateUserCityAction::class);
        // ->add(JwtAuthMiddleware::class);
    });

    $app->group('/cities', function (Group $group) {
        $group->post('', ListCitiesAction::class);
        $group->get('/{id}', ViewCityAction::class);
        $group->put('', CreateCityAction::class);
    })->add(JwtAuthMiddleware::class);

    $app->group('/weather', function (Group $group) {
        $group->get('', ViewWeatherForecastAction::class);
    })->add(JwtAuthMiddleware::class);
};
