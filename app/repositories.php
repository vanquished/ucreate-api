<?php
declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Domain\City\CityRepository;
use App\Infrastructure\Persistence\User\DbUserRepository;
use App\Infrastructure\Persistence\City\DbCityRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(DbUserRepository::class),
        CityRepository::class => \DI\autowire(DbCityRepository::class),
    ]);
};
