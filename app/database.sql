CREATE DATABASE IF NOT EXISTS `ucreate` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

USE `ucreate`;

CREATE TABLE `user` ( `id` INT NOT NULL AUTO_INCREMENT , `username` VARCHAR(100) NOT NULL , `hash` VARCHAR(255) NOT NULL , `auth_token` VARCHAR(360) NOT NULL , PRIMARY KEY (`id`), UNIQUE `username` (`username`)) ENGINE = InnoDB;

CREATE TABLE `ucreate`.`city` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(25) NOT NULL , `country_code` VARCHAR(25) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `ucreate`.`user_city` ( `user_id` INT NOT NULL , `city_id` INT NOT NULL ) ENGINE = InnoDB;

COMMIT;