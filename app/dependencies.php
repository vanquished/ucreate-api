<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Firebase\JWT\JWT;
use App\Application\Middleware\JwtAuthMiddleware;
use App\Infrastructure\Persistence\User\DbUserRepository;
use App\Domain\Weather\WeatherRepositoryFactory;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        PDO::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');
            $db = $settings['db'];

            $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'], $db['user'], $db['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            return $pdo;
        },
        WeatherRepositoryFactory::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');
            return new WeatherRepositoryFactory($settings);
        },
        JwtAuthMiddleware::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');
            return new JwtAuthMiddleware($settings);
        },
    ]);
};
