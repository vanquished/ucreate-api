<?php
declare(strict_types=1);

namespace App\Domain\User;

interface UserRepository
{
    /**
     * @param string $username
     * @param string $hash
     * @return User
     * @throws Exception|UserNotFoundException
     */
    public function add(string $username, string $hash): User;

    /**
     * @param int $id
     * @return User
     * @throws Exception|UserNotFoundException
     */
    public function findById(int $id): User;

    /**
     * @param string $username
     * @return User
     * @throws Exception|UserNotFoundException
     */
    public function findByUsername(string $username): User;

    /**
     * @param int $userId
     * @param int $cityId
     * @return
     * @throws Exception
     */
    public function updateUserCity(int $userId, int $cityId): bool;
}
