<?php
declare(strict_types=1);

namespace App\Domain\Weather;

use App\Domain\DomainException\DomainRecordNotFoundException;

class WeatherNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The weather you requested does not exist.';
}
