<?php
declare(strict_types=1);

namespace App\Domain\Weather;

use App\Api\OpenWeatherMap;
use App\Api\Weatherbit;
use App\Api\WeatherApi;
use App\Infrastructure\Persistence\Weather\ApiOpenWeatherMapRepository;
use App\Infrastructure\Persistence\Weather\ApiWeatherbitRepository;
use App\Infrastructure\Persistence\Weather\ApiWeatherApiRepository;
use GuzzleHttp\Client;
use Exception;

class WeatherRepositoryFactory
{

  /**
   * @var $settings
   */
  private $settings;

  /**
   * @param array $settings
   */
  public function __construct(array $settings)
  {
    $this->settings = $settings;
  }

  /**
   * @param string $type
   * @return WeatherRepository
   * @throws Exception
   */
  public function make(string $type): WeatherRepository
  {
    switch (strtolower($type)) {
      case 'openweathermap':
        $apiSettings = $this->settings['api']['openWeatherMap'];
        $client = new Client([
          'base_uri' => $apiSettings['baseUri'],
          'timeout' => 10
        ]);
        $api = new OpenWeatherMap($client, $apiSettings['apiKey']);
        return new ApiOpenWeatherMapRepository($api);
        break;
      case 'weatherbit':
        $apiSettings = $this->settings['api']['weatherbit'];
        $client = new Client([
          'base_uri' => $apiSettings['baseUri'],
          'timeout' => 10
        ]);
        $api = new Weatherbit($client, $apiSettings['apiKey']);
        return new ApiWeatherbitRepository($api);
        break;
      case 'weatherapi':
        $apiSettings = $this->settings['api']['weatherApi'];
        $client = new Client([
          'base_uri' => $apiSettings['baseUri'],
          'timeout' => 10
        ]);
        $api = new WeatherApi($client, $apiSettings['apiKey']);
        return new ApiWeatherApiRepository($api);
        break;
      default:
        throw new Exception('Factory error');
    }
  }
}