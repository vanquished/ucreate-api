<?php
declare(strict_types=1);

namespace App\Domain\Weather;

use JsonSerializable;

class TemperatureRange implements JsonSerializable
{
    private $high;
    private $low;

    /**
     * @param float $high
     * @param float $low
     */
    public function __construct(float $high, float $low)
    {
        $this->high = $high;
        $this->low = $low;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'high' => $this->high,
            'low' => $this->low
        ];
    }
}
