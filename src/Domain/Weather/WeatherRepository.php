<?php
declare(strict_types=1);

namespace App\Domain\Weather;

interface WeatherRepository
{
  /**
   * @param string $city
   * @param string $countryCode
   * @param int $days
   */
  public function getForecast(string $city, string $countryCode, int $days): Forecast;
}
