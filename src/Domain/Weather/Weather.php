<?php
declare(strict_types=1);

namespace App\Domain\Weather;

use JsonSerializable;

class Weather implements JsonSerializable
{
    private $date;
    private $description;
    private $temperatureRange;

    /**
     * @param string $date
     * @param string $description
     * @param TemperatureRange $temperatureRange
     */
    public function __construct(string $date, string $description, TemperatureRange $temperatureRange)
    {
        $this->date = $date;
        $this->description = $description;
        $this->temperatureRange = $temperatureRange;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'date' => $this->date,
            'description' => $this->description,
            'temperatureRange' => $this->temperatureRange->jsonSerialize()
        ];
    }
}
