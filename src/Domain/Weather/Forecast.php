<?php
declare(strict_types=1);

namespace App\Domain\Weather;

use JsonSerializable;

class Forecast implements JsonSerializable
{
    private $source;
    private $weather;

    /**
     * @param string $source
     * @param array $weather
     * @param TemperatureRange $temperatureRange
     */
    public function __construct(string $source, array $weather)
    {
        $this->source = $source;
        $this->weather = $weather;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'source' => $this->source,
            'weather' => $this->weather
        ];
    }
}
