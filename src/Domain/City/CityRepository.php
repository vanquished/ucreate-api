<?php
declare(strict_types=1);

namespace App\Domain\City;

interface CityRepository
{
    /**
     * @param string $name
     * @return City
     * @throws Exception|CityNotFoundException
     */
    public function add(string $name): City;

    /**
     * @return array
     * @throws Exception|CityNotFoundException
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return City
     * @throws Exception|CityNotFoundException
     */
    public function findById(int $id): City;

    /**
     * @param int $userId
     * @return array
     * @throws Exception
     */
    public function filter(int $userId): array;
}
