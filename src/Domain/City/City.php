<?php
declare(strict_types=1);

namespace App\Domain\City;

use JsonSerializable;

class City implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @param int|null $id
     * @param string $name
     */
    public function __construct(?int $id, string $name, string $countryCode)
    {
        $this->id = $id;
        $this->name = $name;
        $this->countryCode = $countryCode;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'countryCode' => $this->countryCode
        ];
    }
}
