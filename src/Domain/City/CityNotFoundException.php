<?php
declare(strict_types=1);

namespace App\Domain\City;

use App\Domain\DomainException\DomainRecordNotFoundException;

class CityNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The city you requested does not exist.';
}
