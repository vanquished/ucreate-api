<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Exception;

class UpdateUserCityAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = (array) $this->request->getParsedBody();
        if (!$this->validateData($data)) {
            return $this->respond(
                new ActionPayload(400, null, new ActionError('VALIDATION_ERROR', ''))
            );
        }

        $userId = (int) $data['userId'];
        $cityId = (int) $data['cityId'];

        try {
            $success = $this->userRepository->updateUserCity($userId, $cityId);

            if (!$success) {
                return $this->respond(
                    new ActionPayload(500, null, new ActionError('SERVER_ERROR', ''))
                );
            }
        } catch (Exception $e) {
            return $this->respond(
                new ActionPayload(500, null, new ActionError('SERVER_ERROR', ''))
            );
        }

        return $this->respond(new ActionPayload(200));
    }

    private function validateData(array $data): bool
    {
        $error = (array) [];

        if (!isset($data['userId']) || !is_numeric($data['userId'])) {
            $error[] = 'Invalid user id';
        }

        if (!isset($data['cityId']) || !is_numeric($data['cityId'])) {
            $error[] = 'Invalid city id';
        }

        return (count($error) > 0) ? false : true;
    }
}
