<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Exception;

class LoginUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = (array) $this->request->getParsedBody();
        if (!$this->validateData($data)) {
            return $this->respond(
                new ActionPayload(400, null, new ActionError('VALIDATION_ERROR', ''))
            );
        }

        $username = (string) $data['username'];
        $password = (string) $data['password'];
        $hash = password_hash($password, PASSWORD_DEFAULT);

        try {
            $user = $this->userRepository->findByUsername($username);

            if (password_verify($password, $user->getPassword())) {
                $tokenId = base64_encode(random_bytes(32));
                $issuedAt = time();
                $notBefore = $issuedAt;
                $expire = $notBefore + 60000;
                $secretKey = 'secretkey';

                $data = [
                    'iat'  => $issuedAt,
                    'jti'  => $tokenId,
                    'nbf'  => $notBefore,
                    'exp'  => $expire,
                    'userId' => $user->getId()
                ];

                $jwt = JWT::encode(
                    $data,
                    $secretKey,
                    'HS512'
                );
            } else {
                return $this->respond(
                    new ActionPayload(401, null, new ActionError('VALIDATION_ERROR', ''))
                );
            }
        } catch (Exception $e) {
            return $this->respond(
                new ActionPayload(500, null, new ActionError('SERVER_ERROR', ''))
            );
        }

        return $this->respondWithData([
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'apiToken' => $jwt
        ]);
    }

    private function validateData(array $data): bool
    {
        $error = (array) [];

        if (!isset($data['username'])
            || strlen($data['username']) <= 0
            || strlen($data['username']) > 25) {
                $error[] = 'Invalid username';
        }

        if (!isset($data['password']) || strlen($data['password']) <= 0) {
            $error[] = 'Invalid password';
        }

        return (count($error) > 0) ? false : true;
    }
}
