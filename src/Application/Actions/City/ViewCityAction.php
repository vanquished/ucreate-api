<?php
declare(strict_types=1);

namespace App\Application\Actions\City;

use Psr\Http\Message\ResponseInterface as Response;

class ViewCityAction extends CityAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $cityId = (int) $this->resolveArg('id');

        try {
            $city = $this->cityRepository->findById($cityId);
        } catch (Exception $e) {
            if ($e instanceof CityNotFoundException) {
                $responseErrorType = 'RESOURCE_NOT_FOUND';
            } else {
                $responseErrorType = 'SERVER_ERROR';
            }
            return $this->respond(
                new ActionPayload(500, null, new ActionError($responseErrorType, $e->getMessage()))
            );
        }

        return $this->respondWithData($city);
    }
}
