<?php
declare(strict_types=1);

namespace App\Application\Actions\City;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use Psr\Http\Message\ResponseInterface as Response;

class CreateCityAction extends CityAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = (array) $this->request->getParsedBody();
        if (!$this->validateData($data)) {
            return $this->respond(
                new ActionPayload(400, null, new ActionError('VALIDATION_ERROR', ''))
            );
        }

        $name = (string) $data['name'];

        try {
            $city = $this->cityRepository->add($name);
        } catch (Exception $e) {
            return $this->respond(
                new ActionPayload(500, null, new ActionError('SERVER_ERROR', ''))
            );
        }

        return $this->respondWithData($city, 201);
    }

    private function validateData(array $data): bool
    {
        $error = (array) [];

        if (!isset($data['name'])
            || strlen($data['name']) <= 0
            || strlen($data['name']) > 25) {
                $error[] = 'Invalid name';
        }

        return (count($error) > 0) ? false : true;
    }
}
