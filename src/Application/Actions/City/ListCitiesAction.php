<?php
declare(strict_types=1);

namespace App\Application\Actions\City;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use Psr\Http\Message\ResponseInterface as Response;
use Exception;
use App\Domain\City\CityNotFoundException;

class ListCitiesAction extends CityAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = (array) $this->request->getParsedBody();

        try {
            if (isset($data['userId']) && is_numeric($data['userId'])) {
                $userId = (int) $data['userId'];
                $cities = $this->cityRepository->filter($userId);
            } else {
                $cities = $this->cityRepository->findAll();
            }
        } catch (Exception $e) {
            if ($e instanceof CityNotFoundException) {
                $responseErrorType = 'RESOURCE_NOT_FOUND';
            } else {
                $responseErrorType = 'SERVER_ERROR';
            }
            return $this->respond(
                new ActionPayload(500, null, new ActionError($responseErrorType, $e->getMessage()))
            );
        }

        return $this->respondWithData($cities);
    }
}
