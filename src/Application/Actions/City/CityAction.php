<?php
declare(strict_types=1);

namespace App\Application\Actions\City;

use App\Application\Actions\Action;
use App\Domain\City\CityRepository;
use Psr\Log\LoggerInterface;

abstract class CityAction extends Action
{
    /**
     * @var CityRepository
     */
    protected $cityRepository;

    /**
     * @param LoggerInterface $logger
     * @param CityRepository  $cityRepository
     */
    public function __construct(LoggerInterface $logger, CityRepository $cityRepository)
    {
        parent::__construct($logger);
        $this->cityRepository = $cityRepository;
    }
}
