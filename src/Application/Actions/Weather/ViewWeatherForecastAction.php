<?php
declare(strict_types=1);

namespace App\Application\Actions\Weather;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use Psr\Http\Message\ResponseInterface as Response;
use \App\Domain\Weather\WeatherRepositoryFactory;

class ViewWeatherForecastAction extends WeatherAction
{
  /**
   * {@inheritdoc}
   */
  protected function action(): Response
  {
    $query = (array) $this->request->getQueryParams();
    $cities = (string) $query['cities'];
    $countryCodes = (string) $query['countryCodes'];
    $days = (int) $query['days'];
    $source = (string) $query['source'];

    try {
      $repo = $this->repositoryFactory->make($source);
    } catch (Exception $e) {
      return $this->respond(
        new ActionPayload(500, null, new ActionError('SERVER_ERROR', ''))
      );
    }

    $forecast = $repo->getForecast($cities, $countryCodes, $days);

    return $this->respondWithData($forecast);
  }
}
