<?php
declare(strict_types=1);

namespace App\Application\Actions\Weather;

use App\Application\Actions\Action;
use App\Domain\Weather\WeatherRepositoryFactory;
use Psr\Log\LoggerInterface;

abstract class WeatherAction extends Action
{
    /**
     * @var WeatherRepositoryFactory
     */
    protected $repositoryFactory;

    /**
     * @param LoggerInterface $logger
     * @param UserRepository  $userRepository
     */
    public function __construct(LoggerInterface $logger, WeatherRepositoryFactory $repositoryFactory)
    {
        parent::__construct($logger);
        $this->repositoryFactory = $repositoryFactory;
    }
}
