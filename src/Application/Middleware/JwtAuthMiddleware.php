<?php
declare(strict_types=1);

namespace App\Application\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface as Middleware;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response as NewResponse;
use Firebase\JWT\JWT;
use Exception;
use Firebase\JWT\ExpiredException;

class JwtAuthMiddleware implements Middleware
{
    private $settings;

    /**
     * @param array $settings
     */
    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    /**
     * {@inheritdoc}
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        $authHeader = (array) $request->getHeader('Authorization');

        if (count($authHeader) > 0) {
            $authToken = str_replace('Bearer ', '', $authHeader[0]);

            try {
                $secretKey = $this->settings['jwt']['secretKey'];
                $jwt = (array) JWT::decode($authToken, $secretKey, ['HS512']);

                return $handler->handle($request);
            } catch(Exception $e) {
                $response = new NewResponse();

                return $response
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(401);
            }

        } else {
            $response = new NewResponse();

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(401);
        }
    }
}
