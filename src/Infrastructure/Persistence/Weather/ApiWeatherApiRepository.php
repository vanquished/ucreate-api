<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Weather;

use App\Api\WeatherApi;
use App\Domain\Weather\Forecast;
use App\Domain\Weather\TemperatureRange;
use App\Domain\Weather\Weather;
use App\Domain\Weather\WeatherNotFoundException;
use App\Domain\Weather\WeatherRepository;
use Exception;

class ApiWeatherApiRepository implements WeatherRepository
{
  private $api;

  /**
   * @param WeatherApi
   */
  public function __construct(WeatherApi $api)
  {
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public function getForecast(string $city, string $countryCode, int $days): Forecast
  {
    $resp = $this->api->forecast($city, $days, 'M');
    $forecastDays = $resp['forecast']['forecastday'];
    $weather = [];

    foreach ($forecastDays as $forecastDay) {
      $tempRange = new TemperatureRange(
        $forecastDay['day']['maxtemp_c'],
        $forecastDay['day']['mintemp_c']
      );

      $weather[] = new Weather(
        $forecastDay['date'],
        $forecastDay['day']['condition']['text'],
        $tempRange
      );
    }

    $forecast = new Forecast('WeatherAPI', $weather);

    return $forecast;
  }
}
