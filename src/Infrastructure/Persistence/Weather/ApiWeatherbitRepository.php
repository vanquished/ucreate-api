<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Weather;

use App\Api\Weatherbit;
use App\Domain\Weather\Forecast;
use App\Domain\Weather\TemperatureRange;
use App\Domain\Weather\Weather;
use App\Domain\Weather\WeatherNotFoundException;
use App\Domain\Weather\WeatherRepository;
use Exception;

class ApiWeatherbitRepository implements WeatherRepository
{
  private $api;

  /**
   * @param Weatherbit $api
   */
  public function __construct(Weatherbit $api)
  {
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public function getForecast(string $city, string $countryCode, int $days): Forecast
  {
    $resp = $this->api->forecastDaily($city, $countryCode, $days, 'M');
    $forecastData = $resp['data'];
    $weather = [];

    foreach ($forecastData as $forecast) {
      $tempRange = new TemperatureRange(
        $forecast['max_temp'],
        $forecast['low_temp']
      );

      $weather[] = new Weather(
        $forecast['datetime'],
        $forecast['weather']['description'],
        $tempRange
      );
    }

    $forecast = new Forecast('Weatherbit', $weather);

    return $forecast;
  }
}
