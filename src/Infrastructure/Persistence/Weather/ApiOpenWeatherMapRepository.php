<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Weather;

use App\Api\OpenWeatherMap;
use App\Domain\Weather\Forecast;
use App\Domain\Weather\TemperatureRange;
use App\Domain\Weather\Weather;
use App\Domain\Weather\WeatherNotFoundException;
use App\Domain\Weather\WeatherRepository;
use Exception;

class ApiOpenWeatherMapRepository implements WeatherRepository
{
  private $api;

  /**
   * @param OpenWeatherMap $api
   */
  public function __construct(OpenWeatherMap $api)
  {
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public function getForecast(string $city, string $countryCode, int $days): Forecast
  {
    // Calls to /forecast/daily are coming back with invalid API key so we
    // have to get lat & long from /weather, then use /onecall as a work around
    $currentWeatherResp = (array) $this->api->currentWeatherData($city, $countryCode);
    $lon = (float) $currentWeatherResp['coord']['lon'];
    $lat = (float) $currentWeatherResp['coord']['lat'];

    $oneCallResp = $this->api->oneCall($lon, $lat, 'metric', 'current,hourly,minutely');
    $daily = $oneCallResp['daily'];
    $weather = [];

    for ($i = 0; $i < $days; $i++) {
      $tempRange = new TemperatureRange(
        max(array_values($daily[$i]['temp'])),
        min(array_values($daily[$i]['temp']))
      );

      $date = date('Y-m-d', $daily[$i]['dt']);

      $weather[] = new Weather(
        $date,
        $daily[$i]['weather'][0]['description'],
        $tempRange
      );
    }

    $forecast = new Forecast('OpenWeatherMap', $weather);

    return $forecast;
  }
}
