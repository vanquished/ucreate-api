<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\City;

use App\Domain\City\City;
use App\Domain\City\CityNotFoundException;
use App\Domain\City\CityRepository;
use PDO;
use PDOException;
use Exception;

class DbCityRepository implements CityRepository
{
    /**
     * PDO
     */
    private $db;

    /**
     * @param PDO $db
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $name): City
    {
        $stmt = $this->db->prepare("INSERT INTO `city` (`name`) VALUES (:name)");
        $stmt->bindValue(':name', $name);

        if ($stmt->execute()) {
            $id = (int) $this->db->lastInsertId();
            $city = $this->findById($id);
            return $city;
        } else {
            throw new Exception('Unable to execute query');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $stmt = $this->db->prepare("SELECT `id`, `name`, `country_code` FROM `city`");

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll();

            if (count($rows) > 0) {
                $cities = [];

                foreach ($rows as $row) {
                    $id = (int) $row['id'];
                    $name = (string) $row['name'];
                    $countryCode = (string) $row['country_code'];
                    $cities[] = new City($id, $name, $countryCode);
                }

                return $cities;
            } else {
                throw new CityNotFoundException;
            }
        } else {
            throw new Exception('Unable to execute query');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): City
    {
        $stmt = $this->db->prepare("SELECT `id`, `name`, `country_code` FROM `city` WHERE `id` = :id LIMIT 1");
        $stmt->bindValue(":id", $id);

        if ($stmt->execute()) {
            $row = $stmt->fetch();

            if ($row) {
                $id = (int) $row['id'];
                $name = (string) $row['name'];
                $countryCode = (string) $row['country_code'];
                $city = new City($id, $name, $countryCode);
                return $city;
            } else {
                throw new CityNotFoundException;
            }
        } else {
            throw new Exception('Unable to execute query');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function filter(int $userId): array
    {
        $stmt = $this->db->prepare("SELECT `id`, `name`, `country_code` FROM `city` WHERE `id` IN ( SELECT `city_id` FROM `user_city` WHERE `user_id` = :userId )");
        $stmt->bindValue(":userId", $userId);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll();

            if (count($rows) > 0) {
                $cities = [];

                foreach ($rows as $row) {
                    $id = (int) $row['id'];
                    $name = (string) $row['name'];
                    $countryCode = (string) $row['country_code'];
                    $cities[] = new City($id, $name, $countryCode);
                }

                return $cities;
            } else {
                throw new CityNotFoundException;
            }
        } else {
            throw new Exception('Unable to execute query');
        }

        return [];
    }
}
