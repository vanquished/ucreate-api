<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use PDO;
use PDOException;
use Exception;

class DbUserRepository implements UserRepository
{
    /**
     * PDO
     */
    private $db;

    /**
     * @param PDO $db
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $username, string $hash): User
    {
        $stmt = $this->db->prepare("INSERT INTO `user` (`username`, `hash`) VALUES (:username, :hash)");
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':hash', $hash);

        if ($stmt->execute()) {
            $id = (int) $this->db->lastInsertId();
            $user = $this->findById($id);
            return $user;
        } else {
            throw new Exception('Unable to execute query');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): User
    {
        $stmt = $this->db->prepare("SELECT `id`, `username`, `hash` FROM `user` WHERE `id` = :id LIMIT 1");
        $stmt->bindValue(":id", $id);

        if ($stmt->execute()) {
            $row = $stmt->fetch();

            if ($row) {
                $id = (int) $row['id'];
                $username = (string) $row['username'];
                $user = new User($id, $username, '');
                return $user;
            } else {
                throw new UserNotFoundException();
            }
        } else {
            throw new Exception('Unable to execute query');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUsername(string $username): User
    {
        $stmt = $this->db->prepare("SELECT `id`, `username`, `hash` FROM `user` WHERE `username` = :username LIMIT 1");
        $stmt->bindValue(":username", $username);

        if ($stmt->execute()) {
            $row = $stmt->fetch();

            if ($row) {
                $id = (int) $row['id'];
                $username = (string) $row['username'];
                $hash = (string) $row['hash'];
                $user = new User($id, $username, $hash);
                return $user;
            } else {
                throw new UserNotFoundException();
            }
        } else {
            throw new Exception('Unable to execute query');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateUserCity(int $userId, int $cityId): bool
    {
        $stmt = $this->db->prepare("UPDATE `user_city` SET `city_id` = :cityId WHERE `user_id` = :userId");
        $stmt->bindValue(":userId", $userId);
        $stmt->bindValue(":cityId", $cityId);

        if ($stmt->execute()) {
            return true;
        } else {
            throw new Exception('Unable to execute query');
        }
    }
}
