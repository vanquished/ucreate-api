<?php
declare(strict_types=1);

namespace App\Api;

use GuzzleHttp\Client;
use Exception;

class WeatherApi
{
  private $client;
  private $apiKey;

  /**
   * @param Client $client
   * @param string $apiKey
   */
  public function __construct(Client $client, string $apiKey)
  {
    $this->client = $client;
    $this->apiKey = $apiKey;
  }

  /**
   * @param string $city
   * @param int $days
   * @return array
   * @throws Exception
   */
  public function forecast(string $city, int $days): array
  {
    $response = $this->client->request('GET', 'forecast.json', [
      'query' => [
        'q' => $city,
        'days' => $days,
        'key' => $this->apiKey
      ]
    ]);

    if ($response->getStatusCode() === 200) {
      $body = $response->getBody()->getContents();
      return json_decode($body, true);
    } else {
      throw new Exception('API error');
    }
  }
}
