<?php
declare(strict_types=1);

namespace App\Api;

use GuzzleHttp\Client;
use Exception;

class Weatherbit
{
  private $client;
  private $apiKey;

  /**
   * @param Client $client
   * @param string $apiKey
   */
  public function __construct(Client $client, string $apiKey)
  {
    $this->client = $client;
    $this->apiKey = $apiKey;
  }

  /**
   * @param string $city
   * @param string $countryCode
   * @param int $days
   * @param string $units
   * @return array
   * @throws Exception
   */
  public function forecastDaily(string $city, string $countryCode, int $days, string $units): array
  {
    $response = $this->client->request('GET', 'forecast/daily', [
      'query' => [
        'city' => $city,
        'country' => $countryCode,
        'days' => $days,
        'units' => $units,
        'key' => $this->apiKey
      ]
    ]);

    if ($response->getStatusCode() === 200) {
      $body = $response->getBody()->getContents();
      return json_decode($body, true);
    } else {
      throw new Exception('API error');
    }
  }
}
