<?php
declare(strict_types=1);

namespace App\Api;

use GuzzleHttp\Client;
use Exception;

class OpenWeatherMap
{
  protected $client;
  protected $apiKey;

  /**
   * @param Client $client
   * @param string $apiKey
   */
  public function __construct(Client $client, string $apiKey)
  {
    $this->client = $client;
    $this->apiKey = $apiKey;
  }

  /**
   * @param float $lon
   * @param float $lat
   * @param string $metric
   * @param string $exclude
   * @return array
   * @throws Exception
   */
  public function oneCall(float $lon, float $lat, string $units, string $exclude): array
  {
    $response = $this->client->request('GET', 'onecall', [
      'query' => [
        'lat' => $lat,
        'lon' => $lon,
        'units' => $units,
        'exclude' => $exclude,
        'appid' => $this->apiKey
      ]
    ]);

    if ($response->getStatusCode() === 200) {
      $body = $response->getBody()->getContents();

      return json_decode($body, true);
    } else {
      throw new Exception('API error');
    }
  }

  /**
   * @param string $city
   * @param string $countryCode
   * @return array
   * @throws Exception
   */
  public function currentWeatherData(string $city, string $countryCode): array
  {
    $response = $this->client->request('GET', 'weather', [
      'query' => [
        'q' => "{$city},,{$countryCode}",
        'appid' => $this->apiKey
      ]
    ]);

    if ($response->getStatusCode() === 200) {
      $body = $response->getBody()->getContents();
      return json_decode($body, true);
    } else {
      throw new Exception('API error');
    }
  }
}
